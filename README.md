# Frutiger Web Font #

This is the Frutiger font family. Note that not all fonts are loaded by default. See the load flags section. 

### Fonts Included: ###
* Frutiger Black
* Frutiger Black Condensed
* Frutiger Black Italic
* Frutiger Extra Black Condensed
* Frutiger Bold
* Frutiger Bold Condensed
* Frutiger Bold Italic
* Frutiger Condensed
* Frutiger Italic
* Frutiger Light
* Frutiger Light Condensed
* Frutiger Light Italic
* Frutiger Roman
* Frutiger Ultra Black

### Load Flags (LESS): ###

Less has been deprecated.

### Load Flags (SCSS): ###
* $loadFrutigerBlack:true;
* $loadFrutigerBlackCondensed:false;
* $loadFrutigerBlackItalic:false;
* $loadFrutigerExtraBlackCondensed:false;
* $loadFrutigerBold:true;
* $loadFrutigerBoldCondensed:false;
* $loadFrutigerBoldItalic:false;
* $loadFrutigerCondensed:false;
* $loadFrutigerItalic:true;
* $loadFrutigerLight:true;
* $loadFrutigerLightCondensed:false;
* $loadFrutigerLightItalic:false;
* $loadFrutigerRoman:true;
* $loadFrutigerUltraBlack:false;


### How to Load (SCSS) ###

* Include both web-fonts-akzidenz-grotesk and web-fonts in bower
* In your SCSS file, include web-fonts' font loader utility: ```@import '../vendor/web-fonts/font-loader'```
* Import this font: ```@import '../vendor/web-fonts-frutiger/font-family```
* Execute the load: ```@include loadFontFamily()```

If you wish to exclude versions of the font, change the respective load flags to false prior to calling loadFontFamily();